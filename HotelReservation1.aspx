<%@ Page Language="C#" %>
<!DOCTYPE html>
<html>
    <head runat="server">
        <title>Sheraton Hotel.net</title>
    </head>
    <body>
        <h1>
            Welcome to Sheraton Hotel.net
        </h1>
            <p><strong>Tel: (123) 456-7890 </strong></p>
            <p><strong> Nightly Rate: $180.00 CAD </strong></p>
            <p><strong>Check In: 3:00pm EST || Check Out: 12:00pm EST</strong></p>
            <p>Guest Information:</p>
        
        <form id="form1" runat="server">
            <div>
                <asp:Label ID="customerFname1" runat="server">First Name:</asp:Label>
                <asp:TextBox runat="server" id="customerFirstName" placeholder="e.g. Ogonna"/>
                <asp:RequiredFieldValidator runat="server" id="custFname" controltovalidate="customerFirstname" errormessage="Please enter your First Name!" />
            </div>
         <!--   <div>
                <asp:Label ID="customerMname" runat="server">Middle Name:</asp:Label> 
                <input name="name" id="custMname" type="text" placeholder="(optional)">
            </div>-->
             <div>
                 <asp:Label ID="customerLname1" runat="server">Last Name:</asp:Label> 
                 <asp:TextBox runat="server" id="customerLastname" placeholder="e.g. Anaekwe" />
                 <asp:RequiredFieldValidator runat="server" id="custLname" controltovalidate="customerLastname" errormessage="Please enter your Last Name!" />
            </div>
            <div>
                 <asp:Label ID="customerEmail1" runat="server">Email:</asp:Label>
                 <asp:TextBox runat="server" id="customerEmail" placeholder="e.g. ogonna@gmail.com" />
                 <asp:RequiredFieldValidator runat="server" id="custEmail" ControlToValidate="customerEmail" ErrorMessage="Please enter a valid email address!" />
                 <MyCustomControl:RegularExpressionValidator runat="server" id="EmailRegExpValidator" controltovalidate="customerEmail" ValidationExpression="@!#$%^&*)(/\|." />
            </div>
            <div>
                 <asp:label ID="age"> Age:</asp:label>
                 <asp:DropDownList ID="age" runat="server" >  
                 <asp:ListItem>Select age</asp:ListItem>  
                 <asp:ListItem>0-17 </asp:ListItem>  
                 <asp:ListItem>18-30</asp:ListItem>  
                 <asp:ListItem>31-50</asp:ListItem>  
                 <asp:ListItem>51-70</asp:ListItem>  
                 <asp:ListItem>71-90</asp:ListItem>  
                 <asp:ListItem>91+</asp:ListItem>  
                 </asp:DropDownList>
             </div>
          <!--   <div>
                 <asp:Label ID="hotels" runat="server">Hotel:</asp:Label>
                 <asp:DropDownList ID="hotel" runat="server" >  
                 <asp:ListItem>Select hotel</asp:ListItem>  
                 <asp:ListItem>Sheraton Hotel </asp:ListItem>  
                 <asp:ListItem>Trump Hotel</asp:ListItem>  
                 <asp:ListItem>Intercontinental Hotel</asp:ListItem>  
                 <asp:ListItem>Holiday Inn</asp:ListItem>  
                 </asp:DropDownList>
              </div>-->
              <div>
                 <asp:Label ID="transportation" runat="server">Transportation:</asp:Label>
                 <input id="transportation" type="checkbox" name="transportation" />
                 <asp:Label ID="transportation1" runat="server">Rent a Car</asp:Label>
                 <input id="transportation" type="checkbox" name="transportation" />
                 <asp:Label ID="transportation2" runat="server">Valet Service</asp:Label>
                 <input id="transportation" type="checkbox" name="transportation" />
                 <asp:Label ID="transportation3" runat="server">Shuttle Bus</asp:Label>
              </div>
              <div>
                  <asp:Label ID="RoomService" runat="server">Room Service (optional):</asp:Label>
                  <input id="service" type="checkbox" name="roomservice" />
                  <asp:Label ID="RoomService1" runat="server">Breakfast</asp:Label>
                  <input id="service" type="checkbox" name="roomservice" />
                  <asp:Label ID="RoomService2" runat="server">Lunch</asp:Label>
                  <input id="service" type="checkbox" name="roomservice" />
                  <asp:Label ID="RoomService3" runat="server">Dinner</asp:Label>
              </div>
              <div>
                <asp:Label ID="Extras" runat="server">Extras (optional):</asp:Label>
                  <input id="extraOptions" type="checkbox" name="extrasForCustomer" />
                  <asp:Label ID="Extras1" runat="server">Honeymoon Suite (complimentary with valid marriage certificate)</asp:Label>
                  <input id="extraOptions1" type="checkbox" name="extrasForCustomer" />
                  <asp:Label ID="Extras2" runat="server">Spa $50.00 CAD</asp:Label>
                  <input id="extraOptions2" type="checkbox" name="extrasForCustomer" />
                  <asp:Label ID="Extras3" runat="server">Business Suite $70.00 CAD</asp:Label>
                 <input id="extraOptions3" type="checkbox" name="extrasForCustomer" />
                  <asp:Label ID="Extras4" runat="server">Baby Crib (no extra charge and is available upon request)</asp:Label>
             </div>
              <fieldset>
                 <legend>Duration of Stay:</legend> 
                   <asp:RadioButton ID="Length" runat="server" Text="1 Night" GroupName="duration"/>  
                   <asp:RadioButton ID="Length1" runat="server" Text="2 Weeks" GroupName="duration"/>  
                   <asp:RadioButton ID="Length2" runat="server" Text="1 Month" GroupName="duration"/>  
              </fieldset>
           <!--   <fieldset> 
                  <legend>Rating:</legend> 
                    <asp:RadioButton ID="rating" runat="server" Text="1 Star" GroupName="rating"/>  
                    <asp:RadioButton ID="rating1" runat="server" Text="2 Stars" GroupName="rating"/>  
                    <asp:RadioButton ID="rating2" runat="server" Text="3 Stars" GroupName="rating"/>  
                    <asp:RadioButton ID = "rating3" runat="server" Text="4 Stars" GroupName="rating"/>  
                    <asp:RadioButton ID="rating4" runat="server" Text="5 Stars" GroupName="rating"/>  
              </fieldset>-->
              <fieldset>
                  <legend>Payment Method:</legend> 
                    <asp:RadioButton ID="Pmt1" runat="server" Text="Visa" GroupName="payment"/>  
                    <asp:RadioButton ID="Pmt2" runat="server" Text="MasterCard" GroupName="payment"/>  
                    <asp:RadioButton ID="Pmt3" runat="server" Text="AMEX" GroupName="payment"/>  
             </fieldset>
                  <br>
                    <asp:Button runat="server" id="btnSubmitForm" text="Book Reservation!" />
                  </br>
                <p><strong>Thank you for choosing Sheraton Hotel.net!</strong></p>
             </form>
        </body>
</html>